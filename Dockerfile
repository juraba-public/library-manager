FROM nginx:alpine
LABEL authors="juraba"

ENV APP_HOME=.
ENV NUM_VERSION=$version

WORKDIR $APP_HOME
USER 101

COPY --chown=101:101 $APP_HOME/dist/library-manager/browser /usr/share/nginx/html/

CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.template.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'"]
