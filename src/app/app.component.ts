import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterLink, RouterLinkActive, RouterOutlet} from '@angular/router';
import {MainMenuComponent} from "./components/main-menu/main-menu.component";

@Component({
  selector: 'lib-manager-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, MainMenuComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'library-manager';
}

export const environment = {
  apiBackendUrl: `${((window as {[key: string]: any}["env"])["API_BACKEND_URL"] || 'http://localhost:8080')}/`
}
