import { Routes } from '@angular/router';

const videogameRoutes: Routes = [
  { path: 'videogames', loadComponent: () => import('./domains/videogames/videogames.component').then(m => m.VideogamesComponent) },
];

export const routes: Routes = [
  ...videogameRoutes,
  { path: '**', redirectTo: 'videogames'},
];
